const Pangolin = require("../models/pangolin.model.js");

// Create and Save a new pangolin
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    // Create a pangolin
    const pangolin = new Pangolin({
        email: req.body.email,
        password: req.body.password,
        first_name: req.body.first_name,
        family_id: req.body.family_id,
        race_id: req.body.race_id,
        food: req.body.food
    });

    // Save pangolin in the database
    Pangolin.create(pangolin, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the pangolin."
            });
        else res.send(data);
    });
};

// Retrieve all pangolins from the database.
exports.findAll = (req, res) => {
    Pangolin.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving pangolins."
            });
        else res.send(data);
    });
};

// Find a single pangolin with a pangolinId
exports.findOne = (req, res) => {
    Pangolin.findById(req.params.pangolinId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found pangolin with id ${req.params.pangolinId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving pangolin with id " + req.params.pangolinId
                });
            }
        } else res.send(data);
    });
};

// Update a pangolin identified by the pangolinId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Pangolin.updateById(
        req.params.pangolinId,
        new Pangolin(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found pangolin with id ${req.params.pangolinId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating pangolin with id " + req.params.pangolinId
                    });
                }
            } else res.send(data);
        }
    );
};

// Delete a pangolin with the specified pangolinId in the request
exports.delete = (req, res) => {
    Pangolin.remove(req.params.pangolinId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found pangolin with id ${req.params.pangolinId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete pangolin with id " + req.params.pangolinId
                });
            }
        } else res.send({ message: `pangolin was deleted successfully!` });
    });
};

// Delete all pangolins from the database.
exports.deleteAll = (req, res) => {
    Pangolin.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all customers."
            });
        else res.send({ message: `All pangolins were deleted successfully!` });
    });
};

const Race = require("../models/race.model.js");

// Create and Save a new race
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    // Create a race
    const race = new Race({
        name: req.body.name
    });

    // Save race in the database
    Race.create(race, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the race."
            });
        else res.send(data);
    });
};

// Retrieve all races from the database.
exports.findAll = (req, res) => {
    Race.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving families."
            });
        else res.send(data);
    });
};

// Find a single race with a raceId
exports.findOne = (req, res) => {
    Race.findById(req.params.raceId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found race with id ${req.params.raceId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving race with id " + req.params.raceId
                });
            }
        } else res.send(data);
    });
};

// Update a race identified by the raceId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Race.updateById(
        req.params.raceId,
        new Race(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found Family with id ${req.params.raceId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating Family with id " + req.params.raceId
                    });
                }
            } else res.send(data);
        }
    );
};

// Delete a race with the specified raceId in the request
exports.delete = (req, res) => {
    Race.remove(req.params.raceId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found race with id ${req.params.raceId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete race with id " + req.params.raceId
                });
            }
        } else res.send({ message: `race was deleted successfully!` });
    });
};

// Delete all races from the database.
exports.deleteAll = (req, res) => {
    Race.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all customers."
            });
        else res.send({ message: `All families were deleted successfully!` });
    });
};

module.exports = app => {
    const race = require("../controllers/race.controller.js");

    // Create a new race
    app.post("/races", race.create);

    // Retrieve all races
    app.get("/races", race.findAll);

    // Retrieve a single race with raceId
    app.get("/races/:raceId", race.findOne);

    // Update a Customer with raceId
    app.put("/races/:raceId", race.update);

    // Delete a Customer with raceId
    app.delete("/races/:raceId", race.delete);

    // Create a new race
    app.delete("/races", race.deleteAll);
};

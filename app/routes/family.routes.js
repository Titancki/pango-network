module.exports = app => {
    const family = require("../controllers/family.controller.js");

    // Create a new family
    app.post("/families", family.create);

    // Retrieve all families
    app.get("/families", family.findAll);

    // Retrieve a single family with familyId
    app.get("/families/:familyId", family.findOne);

    // Update a Customer with familyId
    app.put("/families/:familyId", family.update);

    // Delete a Customer with familyId
    app.delete("/families/:familyId", family.delete);

    // Create a new family
    app.delete("/families", family.deleteAll);
};

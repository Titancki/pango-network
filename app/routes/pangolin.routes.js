module.exports = app => {
    const pangolin = require("../controllers/pangolin.controller.js");

    // Create a new pangolin
    app.post("/pangolins", pangolin.create);

    // Retrieve all pangolins
    app.get("/pangolins", pangolin.findAll);

    // Retrieve a single pangolin with pangolinId
    app.get("/pangolins/:pangolinId", pangolin.findOne);

    // Update a Customer with pangolinId
    app.put("/pangolins/:pangolinId", pangolin.update);

    // Delete a Customer with pangolinId
    app.delete("/pangolins/:pangolinId", pangolin.delete);

    // Create a new pangolin
    app.delete("/pangolins", pangolin.deleteAll);
};

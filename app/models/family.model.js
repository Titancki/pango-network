const sql = require("./db.js");

const Family = function(family){
    this.name = family.name;
}

Family.create = (newFamily, result) => {
    sql.query("INSERT INTO family SET ?", newFamily, (err, res) => {
        if (err){
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log("created family: ", { id: res.insertId, ...newFamily });
        result(null, { id: res.insertId, ...newFamily });
    })

}

Family.findById =  (familyId, result) => {
    sql.query(`SELECT * FROM family WHERE id = ${familyId}`, (err, res) =>{
        if (err){
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found family: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({ kind: "not_found" }, null);
    })
}

Family.getAll = result => {
    console.log(result)
    sql.query("SELECT * FROM family", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("family: ", res);
        result(null, res);
    });
};

Family.updateById = (id, family, result) => {
    sql.query(
        "UPDATE family SET name = ? WHERE id = ?",
        [family.name, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Family with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated family: ", { id: id, ...family });
            result(null, { id: id, ...family });
        }
    );
};

Family.remove = (id, result) => {
    sql.query("DELETE FROM family WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Family with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted family with id: ", id);
        result(null, res);
    });
};

Family.removeAll = result => {
    sql.query("DELETE FROM family", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} families`);
        result(null, res);
    });
};

module.exports = Family;

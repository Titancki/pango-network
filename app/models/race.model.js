const sql = require("./db.js");

const Race = function(race){
    this.name = race.name;
}

Race.create = (newrace, result) => {
    sql.query("INSERT INTO race SET ?", newrace, (err, res) => {
        if (err){
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log("created race: ", { id: res.insertId, ...newrace });
        result(null, { id: res.insertId, ...newrace });
    })

}

Race.findById =  (raceId, result) => {
    sql.query(`SELECT * FROM race WHERE id = ${raceId}`, (err, res) =>{
        if (err){
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found race: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({ kind: "not_found" }, null);
    })
}

Race.getAll = result => {
    console.log(result)
    sql.query("SELECT * FROM race", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("race: ", res);
        result(null, res);
    });
};

Race.updateById = (id, race, result) => {
    sql.query(
        "UPDATE race SET name = ? WHERE id = ?",
        [race.name, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found race with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated race: ", { id: id, ...race });
            result(null, { id: id, ...race });
        }
    );
};

Race.remove = (id, result) => {
    sql.query("DELETE FROM race WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found race with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted race with id: ", id);
        result(null, res);
    });
};

Race.removeAll = result => {
    sql.query("DELETE FROM race", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} families`);
        result(null, res);
    });
};

module.exports = Race;

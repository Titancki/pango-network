const sql = require("./db.js");

const Pangolin = function(pangolin){
    this.email = pangolin.email;
    this.password = pangolin.password;
    this.first_name = pangolin.first_name;
    this.family_id = pangolin.family_id;
    this.race_id = pangolin.race_id;
    this.food = pangolin.food;

}

Pangolin.create = (newpangolin, result) => {
    sql.query("INSERT INTO pangolin SET ?", newpangolin, (err, res) => {
        if (err){
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log("created pangolin: ", { id: res.insertId, ...newpangolin });
        result(null, { id: res.insertId, ...newpangolin });
    })

}

Pangolin.findById =  (pangolinId, result) => {
    sql.query(`SELECT * FROM pangolin WHERE id = ${pangolinId}`, (err, res) =>{
        if (err){
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found pangolin: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({ kind: "not_found" }, null);
    })
}

Pangolin.getAll = result => {
    console.log(result)
    sql.query("SELECT * FROM pangolin", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("pangolin: ", res);
        result(null, res);
    });
};

Pangolin.updateById = (id, pangolin, result) => {
    sql.query(
        "UPDATE pangolin SET email = ?, password = ?, first_name = ?, race_id = ?, food = ? WHERE id = ?",
        [pangolin.email, pangolin.password, pangolin.first_name, pangolin.race_id, pangolin.food, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found pangolin with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated pangolin: ", { id: id, ...pangolin });
            result(null, { id: id, ...pangolin });
        }
    );
};

Pangolin.remove = (id, result) => {
    sql.query("DELETE FROM pangolin WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found pangolin with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted pangolin with id: ", id);
        result(null, res);
    });
};

Pangolin.removeAll = result => {
    sql.query("DELETE FROM pangolin", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} families`);
        result(null, res);
    });
};

module.exports = Pangolin;
